const Discord = require('discord.js');
const fs = require('fs');
const async = require('async');
const axios = require('axios');

const bot = new Discord.Client();

bot.on('message', async (msg) => {
  if (msg.content.startsWith('/save')) {
    console.log('[CMD]     ' + msg.author.username + ' used ' + msg.content);

    // Args
    let args = msg.content.split(' ').slice(1);

    if (args.length != 2) {
      msg.reply('Usage /save guildID channelID');
      return;
    }

    let messagesToSave = [];
    let attachmentsToSave = [];

    let guildId = args[0];
    let channelId = args[1];

    // Getting channel
    let guild = await bot.guilds.fetch(guildId);
    let channel = guild.channels.cache
      .filter((channel) => channel.type == 'text' && channel.id == channelId)
      .first();

    msg.channel.send('Saving channel ' + channel.name + '...');

    // Fetching messages
    let allMessages = [];
    let size = 0;
    let options = { limit: 50 };
    let lastID = undefined;

    do {
      if (lastID) options.before = lastID;
      let messages = await channel.messages.fetch(options);

      allMessages.push(...messages.array());
      lastID = messages.last().id;
      size = messages.size;

      console.log('Fetching messages...');
    } while (size == 50);

    // Creating folders
    let root = './save/' + channelId + '/';
    if (!fs.existsSync('save')) fs.mkdirSync('save');
    fs.mkdirSync(root);
    fs.mkdirSync(root + 'attachments/');

    allMessages.reverse().forEach(async (message) => {
      let msgObj = {
        timestamp: message.createdTimestamp,
        author: {
          id: message.author.id,
          username: message.author.username,
        },
        content: message.content,
        attachments: [],
      };

      message.attachments.array().forEach((attachment) => {
        let name =
          attachment.id +
          attachment.url.substring(attachment.url.lastIndexOf('.'));
        msgObj.attachments.push(name);

        attachmentsToSave.push({
          id: attachment.id,
          url: attachment.url,
        });
      });

      messagesToSave.push(msgObj);
    });

    // Saving messages
    fs.writeFileSync(root + 'messages.json', JSON.stringify(messagesToSave));

    // Saving attachments
    await async.eachLimit(attachmentsToSave, 5, (attachment, callback) => {
      let name =
        attachment.id +
        attachment.url.substring(attachment.url.lastIndexOf('.'));
      console.log('Downloading attachment ' + name);

      let file = fs.createWriteStream(root + 'attachments/' + name);
      let url = attachment.url;

      axios({
        url,
        method: 'GET',
        responseType: 'stream',
      }).then((response) => {
        response.data.pipe(file);
        callback();
      });
    });

    console.log('Saved channel ' + channel.name + ' as id ' + channelId);
    msg.channel.send('Saved channel ' + channel.name + ' as id ' + channelId);
  } else if (msg.content.startsWith('/load')) {
    // Args
    let args = msg.content.split(' ').slice(1);

    if (args.length != 1) {
      msg.reply('Usage /load id');
      return;
    }

    let id = args[0];

    let root = './save/' + id + '/';

    // Reading json
    let rawdata = fs.readFileSync(root + 'messages.json');
    let messages = JSON.parse(rawdata);

    async.eachLimit(messages, 1, (message, callback) => {
      // Attachements
      let options = {};

      if (message.attachments.length != 0)
        options.files = message.attachments.map(
          (attachment) => root + 'attachments/' + attachment
        );

      // Sending message
      msg.channel
        .send(
          '```asciidoc\n= ' + message.author.username + ' =\n```\n' + message.content,
          options
        )
        .then(() => callback());
    });
  } else if (msg.content.startsWith('/size')) {
    // Args
    let args = msg.content.split(' ').slice(1);

    if (args.length != 1) {
      msg.reply('Usage /size id');
      return;
    }

    let id = args[0];

    let root = './save/' + id + '/';

    // Reading json
    let rawdata = fs.readFileSync(root + 'messages.json');
    let messages = JSON.parse(rawdata);

    msg.channel.send(id + ' has ' + messages.length + ' messages');
  }
});

bot.login('');
